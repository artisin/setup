################################################################################
#       Default Aliases
################################################################################
# Setup in `.zshrc`
# `source $HOME/.aliases`

# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ~="cd ~" # `cd` is probably faster to type though
alias -- -="cd -"

# List only directories
alias listdir="ls -lF ${colorflag} | grep --color=never '^d'"

alias sha1='openssl sha1'

alias path='echo -e ${PATH//:/\\n}'
alias now='date +"%T"'
alias nowtime=now
alias nowdate='date +"%d-%m-%Y"'

# Stop after sending count ECHO_REQUEST packets #
alias ping='ping -c 5'
# Do not wait interval 1 second, go fast #
alias fastping='ping -c 100 -s.2'

alias ports='netstat -tulanp'

# get web server headers #
alias header='curl -I'

# find out if remote server supports gzip / mod_deflate or not #
alias headerc='curl -I --compress'

## shortcut  for iptables and pass it via sudo#
alias ipt='sudo /sbin/iptables'

# display all rules #
alias iptlist='sudo /sbin/iptables -L -n -v --line-numbers'
alias iptlistin='sudo /sbin/iptables -L INPUT -n -v --line-numbers'
alias iptlistout='sudo /sbin/iptables -L OUTPUT -n -v --line-numbers'
alias iptlistfw='sudo /sbin/iptables -L FORWARD -n -v --line-numbers'
alias firewall=iptlist

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'


# Shortcuts
alias dl="cd ~/Downloads"
alias dt="cd ~/Desktop"
alias p="cd ~/projects"
alias g="git"
alias h="history"
alias j="jobs"

# IP addresses
alias ip="dig +short myip.opendns.com @resolver1.opendns.com"
alias localip="ifconfig getifaddr en1"
alias ips="ifconfig -a | perl -nle'/(\d+\.\d+\.\d+\.\d+)/ && print $1'"


# Copy my public key to the clip
alias sshkey="xclip -sel clip < ~/.ssh/id_rsa.pub | printf '=> Public key copied to clipboard.\n'"

# Flush Directory Service cache
alias flush="dscacheutil -flushcache"

# View HTTP traffic
alias sniff="sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""

# File size
alias fs="stat -f \"%z bytes\""

# Jekyll alias
alias jsw='jekyll serve -w'

# alias wget="wget -c"
alias lsd='ls -Fld *(-/DN)'
alias weather="/home/gregf/code/bin/forecast/forecast.rb"
alias ncmpc="ncmpc -c"
alias fixdbus="sudo -s dbus-uuidgen --ensure"

# Apps
alias v="vim"
alias o="open"
alias s="subl"
alias subl="subl"

# Reload the shell (i.e. invoke as a login shell)
alias reloadshell="exec $SHELL -l"

# Concatenate and print content of files (add line numbers)
alias catn="cat -n"

# Kill all the tabs in Chrome to free up memory
# [C] explained: http://www.commandlinefu.com/commands/view/402/exclude-grep-from-your-grepped-output-of-ps-alias-included-in-description
alias chromekill="ps ux | grep '[C]hrome Helper --type=renderer' | grep -v extension-process | tr -s ' ' | cut -d ' ' -f2 | xargs kill"

alias tlog='tail -f log/development.log'
alias rst='touch tmp/restart.txt'
alias giti="vim .gitignore"
alias g='gthumb'
alias burniso='wodim -v dev=/dev/cdrw'
alias burndvdiso='growisofs -speed=8 -dvd-compat -Z /dev/dvdrw=$1'
alias biosinfo='sudo dmidecode'
alias f-spot='dbus-launch f-spot'
alias gnp="git-notpushed"
alias k="killall"
alias poweroff='sudo poweroff now'
alias reboot='sudo reboot'
alias lsnoext="ls | grep -v '\.'"
alias ipager='k ipager; sleep 1; ipager &'
alias gis="git status | grep --color=always '^[^a-z]\+\(new file:\|modified:\)' | cut -d'#' -f2-"
alias ports='netstat --inet -pln'

alias la="ls -la"
alias srm="sudo rm -rf"

# Git shit
# push
alias gpush="git push -u origin"
alias gpb="git push -u origin"
# Gits Smart https://github.com/geelen/git-smart
# Smart-pull
alias gsp="git smart-pull"
alias smartp="git smart-pull"
# smart-merge
alias gsm="git smart-merge"
alias smartm="git smart-merge"
# smart-log
alias gsl="git smart-log"
alias smartl="git smart-log"

## Git extras
# branch
alias gb="git create-branch"
alias gcbranch="git create-branch"
alias gcb="git create-branch"
alias gdbranch="git delete-branch"
alias gDb="git delete-branch"
# rebase
alias grb="git psykorebase"
alias grmb="git psykorebase --continue"

# Pull master
alias gpm="git pull origin master"
