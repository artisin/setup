#!/bin/bash
## Linux setup. Still working on fine tuning, but its operational or I think it is.

# Current working directory
cwd=$(pwd)

### Echo Notification
fancy_echo() {
  printf "\n%b\n" "$1"
}
### Get config
get_setup() {
  if [[ -f ${cwd}/config/${1}/setup.sh ]]; then
    source ${cwd}/config/${1}/setup.sh
  fi
}
### Run Setup
run_setup() {
  if [[ -f ${cwd}/config/${1}/setup.sh ]]; then
    sudo bash ${cwd}/config/${1}/setup.sh
  fi
}

start() {
  sudo gnome-terminal -e "$@"
}

# printf ${cwd}/.dotfiles/zsh
sudo ln -s -f ${cwd}/.dotfiles/zsh ~/.zsh
sudo ln -s -f ${cwd}/.dotfiles/zsh/zshrc ~/.zshrc


