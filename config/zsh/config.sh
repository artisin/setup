#!/bin/bash

# Current working directory
cwd=$(pwd)

sudo ln -s -f ${cwd}/.dotfiles/zsh ~/.zsh
sudo ln -s -f ${cwd}/.dotfiles/zsh/zshrc ~/.zshrc
sudo ln -s -f ${cwd}/.dotfiles/zsh/zshenv ~/.zshenv
sudo ln -s -f ${cwd}/.dotfiles/zsh/zprofile ~/.zprofile