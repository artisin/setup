#!/bin/bash

#Sublime Text 3 DEV Linux Bash Installer
#Created by esteban@attitude.cl
#Licensed under WTFPL (Do What The Fuck You Want To Public License)
#http://en.wikipedia.org/wiki/WTFPL

# DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                   Version 2, December 2004
#
#Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
#Everyone is permitted to copy and distribute verbatim or modified
#copies of this license document, and changing it is allowed as long
#as the name is changed.
#
#           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

#Usage:
#	chmod +x ./st3installdev && sudo ./st3installdev
#You can upgrade an installation made by this same script, using this
#very same script too ;)

### Echo Notification
fancy_echo() {
  printf "\n%b\n" "$1"
}

# Current working directory
cwd=$(pwd)
INSTALLATION_DIR="/opt/sublime_text"
SUBL_DIR="$HOME/.config/sublime-text-3/Packages/User"

#Remove old
sudo rm -rf $INSTALLATION_DIR/bin
sudo rm -rf $HOME/.config/sublime-text-3

#Bulid URL
STDOWNLOADURLLIKE="http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_"
STURLTOCHECK="`wget -qO- http://www.sublimetext.com/3dev`"

#Home del (http://stackoverflow.com/questions/7358611/bash-get-users-home-directory-when-they-run-a-script-as-root)
USER_HOME=$(getent passwd $SUDO_USER | cut -d: -f6)

fancy_echo "Sublime Text 3 Batch Installer/Upgrader "

#URL distinta segun sistema 64 o 32 bits
if [ "i686" = `uname -m` ]; then
	#http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_3018_x32.tar.bz2
	fancy_echo "System x86 detected."
	#URLS="`grep -Po '(?<=href=").*?(?=">)' <<<"$STURLTOCHECK"`"
	URL="`grep -Po '(?<=href="http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_)(.*?)(\w+x32.tar.bz2)(?=">)' <<<"$STURLTOCHECK"`"
elif [ "x86_64" = `uname -m` ]; then
	#http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_3018_x64.tar.bz2
	fancy_echo "System x86_64 detected."
	URL="`grep -Po '(?<=href="http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_)(.*?)(\w+x64.tar.bz2)(?=">)' <<<"$STURLTOCHECK"`"
fi

#URL format
THEFILE="sublime_text_3_build_$URL"
THEDOWNLOADURL="$STDOWNLOADURLLIKE$URL"

fancy_echo "Downloadind and Installing $THEDOWNLOADURL ..."

#Instalamos Sublime Text 3
  fancy_echo "Creating temp location on /var/cache/..."
  mkdir -p /var/cache/sublime-text-3
  cd /var/cache/sublime-text-3

#A la flash-installer
APT_PROXIES=$(apt-config shell http_proxy Acquire::http::Proxy https_proxy Acquire::https::Proxy ftp_proxy Acquire::ftp::Proxy)
if [ -n "$APT_PROXIES" ]; then
	eval export $APT_PROXIES
fi

fancy_echo "Downloading file to temp location..."
#http://unix.stackexchange.com/questions/37507/what-does-do-here
  :> wgetrc
  echo "noclobber = off" >> wgetrc
  echo "dir_prefix = ." >> wgetrc
  echo "dirstruct = off" >> wgetrc
  echo "verbose = on" >> wgetrc
  echo "progress = bar:default" >> wgetrc
  echo "tries = 3" >> wgetrc
  WGETRC=wgetrc wget --continue -O "$THEFILE" "$THEDOWNLOADURL" \
  	|| fancy_echo "Download failed!!!!"
  	rm -f wgetrc
  fancy_echo "Download completed."

fancy_echo "Unpacking files on temp location..."
  tar xvf "$THEFILE" || fancy_echo "Cannot unpack downloaded file."

fancy_echo "Installing Sublime Text 3 into /opt/sublime_text_3/" 
  mkdir -p /opt/sublime_text_3/
  cp -rf "/var/cache/sublime-text-3/sublime_text_3/"* /opt/sublime_text_3/

fancy_echo "Creating symlinks to /usr/bin/sublime_text"
  ln -sf /opt/sublime_text_3/sublime_text /usr/bin/sublime_text
  ln -sf /opt/sublime_text_3/sublime_text /usr/bin/sublime-text-3
  ln -sf /opt/sublime_text_3/sublime_text /usr/bin/st3
  ln -sf /opt/sublime_text_3/sublime_text /usr/bin/subl
  ln -sf /opt/sublime_text_3/sublime_text /usr/bin/sublime

fancy_echo "Creating symlinks for icons on /usr/share/icons/hicolor/.."
  ln -sf /opt/sublime_text_3/Icon/128x128/sublime-text.png /usr/share/icons/hicolor/128x128/apps/sublime-text-3.png
  ln -sf /opt/sublime_text_3/Icon/256x256/sublime-text.png /usr/share/icons/hicolor/256x256/apps/sublime-text-3.png
  gtk-update-icon-cache /usr/share/icons/hicolor

fancy_echo -e "\e[7m*** Cleaning up temp unpacked files...\e[0m"
  rm -rf "/var/cache/sublime-text-3/sublime_text_3/"
  rm -rf "/var/cache/sublime-text-3/"*.bz2

#Create .desktop
fancy_echo "Creating .desktop file (for easy launch and associate to Sublime Text 3)..."
fancy_echo "[Desktop Entry]
Version=1.0
Type=Application
Name=Sublime Text 3 DEV
GenericName=Text Editor
Comment=Sophisticated text editor for code, markup and prose
#Exec=/opt/sublime_text_3/sublime_text %F
Exec=subl %F
Terminal=false
MimeType=text/plain;text/x-chdr;text/x-csrc;text/x-c++hdr;text/x-c++src;text/x-java;text/x-dsrc;text/x-pascal;text/x-perl;text/x-python;application/x-php;application/x-httpd-php3;application/x-httpd-php4;application/x-httpd-php5;application/xml;text/html;text/css;text/x-sql;text/x-diff;x-directory/normal;inode/directory;
Icon=sublime-text-3
Categories=TextEditor;Development;
StartupNotify=true
Actions=Window;Document;

[Desktop Action Window]
Name=New Window
Exec=/opt/sublime_text_3/sublime_text -n
OnlyShowIn=Unity;

[Desktop Action Document]
Name=New File
Exec=/opt/sublime_text_3/sublime_text --command new_file
OnlyShowIn=Unity;" > /usr/share/applications/"Sublime Text 3.desktop"

#http://superuser.com/questions/93385/run-part-of-a-bash-script-as-a-different-user
#http://superuser.com/questions/195781/sudo-is-there-a-command-to-check-if-i-have-sudo-and-or-how-much-time-is-left
CAN_I_RUN_SUDO=$(sudo -n uptime 2>&1|grep "load"|wc -l)
if [ ${CAN_I_RUN_SUDO} -gt 0 ]; then
	#Instalar Package Control https://sublime.wbond.net/installation
	fancy_echo "Installing Package Control (check https://sublime.wbond.net/ for more awesomeness)..."
	sudo -u $SUDO_USER mkdir -p "$USER_HOME/.config/sublime-text-3/Installed Packages"
	sudo -u $SUDO_USER wget -O "$USER_HOME/.config/sublime-text-3/Installed Packages/Package Control.sublime-package" "https://sublime.wbond.net/Package%20Control.sublime-package"
else
	#No podemos correr sudo
	echo ''
fi

fancy_echo "Opening Sublime so you can enter your license..."
  sudo -u $SUDO_USER subl
  
fancy_echo "Do you enter your license?"
select yn in "Yes" "No"; do
    case $yn in
        Yes )killall subl; break;;
        No ) killall subl; break;;
    esac
done   


fancy_echo "Adding worthy sublime plugins ..."
  if [[ -f ${cwd}/config/sublime/User/"Package Control.sublime-settings" ]]; then
    sudo cp ${cwd}/config/sublime/User/"Package Control.sublime-settings" "$SUBL_DIR/"
  fi

fancy_echo "Opening Sublime To Invoke The Installation Of Said Pacakges ..."
  subl

# watchs package controll file to see if complete rather then timeout
declare PACKAGES
fancy_echo "DO NOT CLOSE Sublime Installing Packages ..."
  sleep 25
  while true; do
   #cat $HOME/Packages\ Control.sublime-package 
   PACKAGES=`cat $HOME/.config/sublime-text-3/Packages/User/Package\ Control.sublime-settings |  egrep  in_process_packages -a -A 2  |  xargs`
   if [ "$PACKAGES" == "in_process_packages: [ ]," ]; then
     fancy_echo "Sublime Packages Complete ... Now closing ..."
     break
   fi
   printf "."
   sleep 2
  done

fancy_echo "Coping setting on over ..."
  if [[ -f ${cwd}/config/sublime/User/"Package Control.sublime-settings" ]]; then
    sudo cp -a ${cwd}/config/sublime/User/. "$SUBL_DIR/"
  fi

# fancy_echo "Would you like to install tern?"
# select yn in "Yes" "No"; do
#     case $yn in
#         Yes )killall subl; break;;
#         No ) killall subl; break;;
#     esac
# done   
