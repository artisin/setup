# KDE Setup
While this can be automated I've been told it can be somewhat hazardous. So the visual shit and setting we will have to do manually but the keyboard shortcuts can be imported.

### Application Apperance
+ Syle  
  + Widget Style -QtCurve
+ Colors
  + Scheme - Oxygen Platinum

### Workspace Apperance
+ Window Decorations
  + Oxygen
+  Cursor Theme
  +  redglass
+  Desktop Theme
  +  Eleonora
+ Splash Screen
  + Simple

### Pithos Key Binding
+ Play/pause
  + RA: net.kevinmehall.Pithos
  + RO: /net/kevinmehall/Pithos
  + Funk: net.kevinmehall.Pithos.PlayPause
+ Next
  + RA: net.kevinmehall.Pithos
  + RO: /net/kevinmehall/Pithos
  + Funk: net.kevinmehall.Pithos.SkipSong

### Spotify Key Binding
```
"dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause" XF86AudioPlay
"dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Stop" XF86AudioStop
"dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next" XF86AudioNext
"dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous" XF86AudioPrevious
```
