# ATI Graphic Card Setup
Set up manually, download/install links in order.

+ fglrx-core_14.500-0ubuntu1_amd64_UB_14.01.deb
    * http://www2.ati.com/drivers/linux/fglrx-core_15.300-0ubuntu1_amd64_UB_14.01.deb
+ fglrx_14.500-0ubuntu1_amd64_UB_14.01.deb
    * http://www2.ati.com/drivers/linux/fglrx_15.300-0ubuntu1_amd64_UB_12.04.4.deb
+ fglrx-dev_14.500-0ubuntu1_amd64_UB_14.01.deb
    * http://www2.ati.com/drivers/linux/fglrx-dev_15.300-0ubuntu1_amd64_UB_14.01.deb
+ fglrx-amdcccle_14.500-0ubuntu1_amd64_UB_14.01.deb
    * http://www2.ati.com/drivers/linux/fglrx-amdcccle_15.300-0ubuntu1_amd64_UB_14.01.deb