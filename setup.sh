#!/bin/bash
## Linux setup. Still working on fine tuning, but its operational or I think it is.

# Current working directory
cwd=$(pwd)

### Echo Notification
fancy_echo() {
  printf "\n%b\n" "$1"
}
### Get config
get_setup() {
  if [[ -f ${cwd}/config/${1}/setup.sh ]]; then
    source ${cwd}/config/${1}/setup.sh
  fi
}
### Run Setup
run_setup() {
  if [[ -f ${cwd}/config/${1}/setup.sh ]]; then
    sudo bash ${cwd}/config/${1}/setup.sh
  fi
}

### Check disto
if ! grep -qiE 'wheezy|jessie|precise|trusty' /etc/os-release; then
  fancy_echo "Sorry! we don't currently support that distro."
  exit 1
fi

### Update package manager if needed
fancy_echo "Updating system packages ..."
  if command -v aptitude >/dev/null; then
    fancy_echo "Using aptitude ..."
  else
    fancy_echo "Installing aptitude ..."
    sudo apt-get install -y aptitude
  fi

### Update
fancy_echo "Updating Linux ..."
  sudo aptitude update

### Upgrade
fancy_echo "Safe-Upgrading Linux ..."
  sudo aptitude safe-upgrade

### Upgrade
fancy_echo "Full-Upgrading Linux ..."
  sudo aptitude full-upgrade

### Chrome
fancy_echo "Installing Chrome-beta ..."
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
  sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
  sudo aptitude update
  sudo aptitude install -y google-chrome-beta

### Curl
fancy_echo "Installing curl ..."
  sudo aptitude install -y curl
  sudo aptitude install -y libcurl3 libcurl3-dev php5-curl

### Git
fancy_echo "Installing git, for source control management ..."
  sudo aptitude install -y git

### Node
fancy_echo "Installing Node ..."
  curl -sL https://deb.nodesource.com/setup | sudo bash -
  sudo aptitude install -y nodejs

### NVM
fancy_echo "Installing Node Version Manager ..."
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash

### Ruby
fancy_echo "Installing base ruby build dependencies ..."
  sudo aptitude install build-dep -y ruby1.9.3

### Gem
fancy_echo "Installing libraries for common gem dependencies ..."
  sudo aptitude install -y libxslt1-dev libcurl4-openssl-dev libksba8 libksba-dev libqtwebkit-dev libreadline-dev

### Vim
fancy_echo "Installing vim ..."
  sudo aptitude install -y vim-gtk

### Ctags
fancy_echo "Installing ctags, to index files for vim tab completion of methods, classes, variables ..."
  sudo aptitude install -y exuberant-ctags

### Tmux
fancy_echo "Installing tmux, to save project state and switch between projects ..."
  sudo aptitude install -y tmux

fancy_echo "Installing watch, to execute a program periodically and show the output ..."
  sudo aptitude install -y watch

### NPM
fancy_echo "Installing NPM ..."
  sudo aptitude install -y npm
fancy_echo "Installing NPM: npm-check ..."
  npm install -g npm-check
fancy_echo "Installing NPM: node-inspector ..."
  sudo npm install -g node-inspector
fancy_echo "Installing NPM: iron-node ..."
  sudo npm install -g iron-node
fancy_echo "Installing NPM: babel-node-debug ..."
  sudo npm install -g babel-node-debug
fancy_echo "Installing NPM: coffee-script ..."
  sudo npm install -g coffee-script
fancy_echo "Installing NPM: eslint + babel-eslint ..."
  sudo npm install -g eslint babel-eslint
fancy_echo "Installing NPM: coffeelint ..."
  sudo npm install -g coffeelint
fancy_echo "Installing NPM: Gulp ..."
  sudo npm install -g gulp

# Git addons
# extras
fancy_echo "Installing git extas"
  curl -sSL http://git.io/git-extras-setup | sudo bash /dev/stdin
# git-smart
fancy_echo "Installing git-smart"
  sudo gem install git-smart
  git config --global color.ui always


# Install go
# fancy_echo "Installing Golang and GVM..."
#   zsh < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)
#   sudo aptitude install bison
#   gvm install go1.4
#   gvm use go1.4 [--default]

silver_searcher_from_source() {
  git clone git://github.com/ggreer/the_silver_searcher.git /tmp/the_silver_searcher
  sudo aptitude install -y automake pkg-config libpcre3-dev zlib1g-dev liblzma-dev
  sh /tmp/the_silver_searcher/build.sh
  cd /tmp/the_silver_searcher
  sh build.sh
  sudo make install
  cd
  rm -rf /tmp/the_silver_searcher
}

if ! command -v ag >/dev/null; then
  fancy_echo "Installing The Silver Searcher (better than ack or grep) to search the contents of files ..."
  if aptitude show silversearcher-ag &>/dev/null; then
    sudo aptitude install silversearcher-ag
  else
    silver_searcher_from_source
  fi
fi
### end linux-components/silver-searcher

### Grep
fancy_echo "Installing grep ..."
  sudo aptitude install -y grep

### Docker
fancy_echo "Installing Docker..."
  curl -sSL https://get.docker.com/ | sh
  sudo usermod -aG docker $USER

### virtualbox
fancy_echo "Installing Virtualbox ..."
  sudo aptitude install -y linux-headers-$(uname -r)
  sudo dpkg-reconfigure virtualbox-dkms
  sudo aptitude install -y virtualbox-qt

### Vagrant
fancy_echo "Installing Vagrant ..."
  sudo aptitude install -y vagrant

### Meteor
fancy_echo "Installing Meteor ..."
  curl https://install.meteor.com/ | sh

### gitg
fancy_echo "Installing Gitg ..."
  sudo aptitude install gitg

### Htop
fancy_echo "Installing Htop ..."
  sudo aptitude install htop

### Redshift
fancy_echo "Installing Redshift ..."
  sudo aptitude install redshift redshift-gtk

### ImmageMagic
fancy_echo "Installing ImageMagick, to crop and resize images ..."
  sudo aptitude install -y imagemagick

### Filezilla
fancy_echo "Installing Filezilla ..."
  sudo add-apt-repository ppa:n-muench/programs-ppa
  sudo aptitude update
  sudo aptitude install filezilla

### VLC
fancy_echo "Installing VLC ..."
  #prbly already installed but hell.
  sudo aptitude install vlc
  sudo aptitude install libdvdcss2

### Shutter
fancy_echo "Installing Web-cam recorder ..."
  sudo aptitude install shuttergnome-web-photo

### TypeCatcher
fancy_echo "Installing Typecatcher ..."
  sudo aptitude install typecatcher
 
### Gimp
fancy_echo "Installing Gimp ..."
  sudo aptitude install gimp

### Inkscape
fancy_echo "Installing Inkscape ..."
  sudo aptitude install inkscape

### Pithos
fancy_echo "Installing Pithos For Pandora Radio ..."
  sudo add-apt-repository ppa:pithos/ppa 
  sudo aptitude update
  sudo aptitude install pithos

### Spotify
fancy_echo "Installing Spotify ..."
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886
  echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
  sudo aptitude update
  sudo aptitude install spotify-client

### Simple Screen Recorder
fancy_echo "Installing Simple Screen Recorder ..."
  sudo add-apt-repository ppa:maarten-baert/simplescreenrecorder
  sudo aptitude update
  sudo aptitude install simplescreenrecorder

### web-cam recorder
fancy_echo "Installing Web-cam recorder (guvcview) ..."
  sudo add-apt-repository ppa:pj-assis/ppa
  sudo aptitude update
  sudo aptitude install guvcview

### Banish
fancy_echo "Installing banish404 ..."
  sudo add-apt-repository ppa:fossfreedom/packagefixes
  sudo aptitude update
  sudo aptitude install banish404
  # run to remove and dups
  sudo banish404

### TODO
# fancy_echo "Installing your personal additions from ~/custom ..."
#   if [[ -f ~/.local ]]; then
#     source ~/.laptop.local
#   fi
### end common-components/personal-additions


### ZSH
fancy_echo "Installing zsh ..."
  sudo aptitude install -y zsh

### Oh-my-zsh
fancy_echo "Do you wish to install oh-my-zsh?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) run_setup "oh-my-zsh"; break;;
        No ) break;;
    esac
done

### zsh Dotfile
# fancy_echo "Do you wish to config zsh from the .dotfiles?"
# select yn in "Yes" "No"; do
#     case $yn in
#         Yes ) run_setup "zsh"; break;;
#         No ) break;;
#     esac
# done

### Folders
fancy_echo "Do you wish to create some default desktop folders?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) run_setup "folders"; break;;
        No ) break;;
    esac
done


### Gitub config
fancy_echo "Do you wish to configure Github?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) get_setup "git"; git config --global user.name ${gitUser}; git config --global user.email ${gitEmail};  break;;
        No ) break;;
    esac
done
  
### Sublime setup
fancy_echo "Do you wish to install and configure Sublime?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) run_setup "sublime"; break;;
        No ) break;;
    esac
done

### Finial Update
fancy_echo "Finial Update, almost done !!!"
  sudo aptitude update

### Reboot
fancy_echo "Do you wish to reboot?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo reboot; break;;
        No ) exit;;
    esac
done
